import Home from './components/Home';
import About from './components/About';
import Poster from './components/Poster';

export default {
    mode: 'history',

    routes: [
        {
            path: '/',
            component: Home
        },
        {
            path: '/about',
            component: About,
            name: 'about'
        },
        {
            path: '/poster',
            component: Poster,
            name: 'poster'
        },

    ]
};