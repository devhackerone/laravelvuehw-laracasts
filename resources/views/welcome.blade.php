<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Laravel</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet">

        <!-- Styles -->
        <style>
            html, body {
                background-color: #fff;
                color: #636b6f;
                font-family: 'Nunito', sans-serif;
                font-weight: 200;
                height: 100vh;
                margin: 10px;
            }
        </style>
    </head>
    <body>
        <div id="app">
            <header>
                <h1>Laravue</h1>
            </header>
            <div class="primary">
                <router-view></router-view>
            </div>

            <hr>

            <router-link to="/">Home</router-link>
            <router-link to="/about">About</router-link>
            <router-link to="/poster">Poster</router-link>
        </div>
    <script src="/js/app.js"></script>
    </body>
</html>
